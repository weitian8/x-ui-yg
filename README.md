### 纯IPV4/纯IPV6的VPS直接运行一键脚本

```
wget -N https://gitlab.com/rwkgyg/x-ui-yg/raw/main/install.sh && bash install.sh
```

--------------------------------------------------------------------------------------------------------------------------------------------------

### 相关更新说明请查看[甬哥博客](https://ygkkk.blogspot.com/2022/02/githubx-uitgacmex-uiipv4ipv6v4v6vpsvaxi.html)

---------------------------------------------------------------------------------------------------------------------------------------------------
### [Github地址](https://github.com/yonggekkk/x-ui-yg)
### 本项目基于上游X-UI项目进行略微的功能改动！后续将紧跟上游X-UI版本更新！在此感谢[vaxilu](https://github.com/vaxilu/x-ui)，[FranzKafkaYu](https://github.com/FranzKafkaYu/x-ui)，[MHSanaei](https://github.com/MHSanaei/3x-ui)，[qist](https://github.com/qist/xray-ui)、[misaka](https://gitlab.com/Misaka-blog/x-ui-msk)等各位为此项目做出贡献

----------------------------------------------------------------------------------------------------------------------------------------------

### 你可能喜欢

### [关于x-ui的质疑说明](https://ygkkk.blogspot.com/2022/06/github.html)

### [全网首发：x-ui面版登录设置解析图，代理节点参数解析图，纯IPV4、纯IPV6登录X-UI面板的多种情况，以及cloudflare小黄云及端口的来龙去脉](https://ygkkk.blogspot.com/2022/03/x-uiipv4ipv6x-uicloudflare.html)


